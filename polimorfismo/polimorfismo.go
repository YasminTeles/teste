package main

import "fmt"

type Yasmin struct{
	inferno string
	n int
}

type Mouro interface{
	ds()
	getInferno() string
}

func main() {
	var i interface{} = "hello"
	y := Yasmin{"VNP", 30}

	Jorge(y)
	s := i.(string)
	fmt.Println(s)

	s, ok := i.(string)
	fmt.Println(s, ok)

	f, ok := i.(float64)
	fmt.Println(f, ok)

	//	f = i.(type) // panic
	//	fmt.Println(f)
}

func (y Yasmin) ds (){
	fmt.Println("ds ", y.inferno)
}

func (y Yasmin) getInferno() string{
	return y.inferno
}

func Jorge (a interface{}){
	s := a.(Mouro)
	s.ds()
	fmt.Println("Jorge ", s.getInferno())
}
