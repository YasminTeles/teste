package subsequencia

import "testing"

func TestEncontarSubsequencias(t *testing.T) {
	for _, c := range []struct {
		in string
		sequencias []caracteres
	}{
		{"", []caracteres{{1, "g"}}},
		{"glooooobo.com", []caracteres{{1, "g"},
													{1,"l"},
													{5, "o"},
													{1, "b"},
													{1, "o"}}},
		{"abcd", []caracteres{{1, "a"},
											{1,"b"},
											{1, "c"},
											{1,"d"}}},
		{"aaaaadgdfgd", []caracteres{{5, "a"},
												{1,"d"},
												{1, "g"},
												{1, "d"},
												{1, "f"},
												{1, "g"},
												{1, "d"},}},
		{"daaaasfsadggggjjjjjj", []caracteres{{1, "d"},
															{4, "a"},
															{1, "s"},
															{1, "f"},
															{1, "s"},
															{1, "a"},
															{1, "d"},
															{4, "g"},
															{6, "j"},}},
	}{
		var got []caracteres
		got = EncontarSubsequencias(c.in)
		//if got != c.sequencias {

		//}
		t.Error("Subsequencia ", EncontarSubsequencias("Gollllca"))
	}
}

func TestMaiorSubsequencia(t *testing.T) {
	for _, c := range []struct {
		in, out string
	}{
		{"", ""},
		{"glooooobo.com", "ooooo"},
		{"abcd", ""},
		{"aaaaadgdfgd", "aaaaa"},
		{"daaaasfsadggggjjjjjj", "jjjjjj"},
	}{
		got:= MaiorSubsequencia(c.in)
		if got != c.out {
			t.Errorf("MaiorSubsequencia(%v): %s != %s", EncontarSubsequencias(c.in), got, c.out)
		}
	}
}
