package subsequencia

import (
	"strings"
)

type caracteres struct{
	tamanho int
	caracter string
}

/**
* Mapeia todas as subsequencias de uma cadeia de caracteres
*/
func EncontarSubsequencias(frase string) []caracteres{
	subsequencias := make([]caracteres, 0)
	if len(frase) > 0 {
		subsequencias = append(subsequencias, caracteres{1, string(frase[0])})
		i := 0
		for j:= 1; j < len(frase); j++ {
			if subsequencias[i].caracter == string(frase[j]) {
				subsequencias[i].tamanho++
			} else {
				i++
				subsequencias = append(subsequencias, caracteres{1, string(frase[j])})
			}
		}
	}
	return subsequencias
}

/**
* Encontra a maior subsequecia de uma cadeia de caracteres
*/
func MaiorSubsequencia(frase string) string{
	subsequencias := EncontarSubsequencias(frase)
	if subsequencias == nil || len(subsequencias) <= 0{
		return ""
	}

	maiorTamanho := 0
	var indice int
	for i, subsequencia := range subsequencias{
		if subsequencia.tamanho > maiorTamanho{
			maiorTamanho = subsequencia.tamanho
			indice = i
		}
	}
	if maiorTamanho == 1 {
		return ""
	} else {
		return strings.Repeat(subsequencias[indice].caracter, subsequencias[indice].tamanho)
	}
}
