package main

import (
	"time"
	"fmt"
	"github.com/YasminGomes/teste/MaiorSubsequencia/subsequencia"
)

func main() {
	start := time.Now()
	frase := "gloooooblo.com"
	subsequencia := subsequencia.MaiorSubsequencia(frase)
	fmt.Println("Maior subsequencia de %s é %s", frase, subsequencia)
	fmt.Println("Tempo de execução: ", time.Since(start).Seconds(), "segundos")
}
