package main

import "fmt"

type ListaGenerica []interface{}
type Dado struct {
	a int
	b int
}

type Lista []Dado

func (lista *ListaGenerica) RemoverIndice(indice int) interface{}{
	l := *lista
	removido := l[indice]
	*lista = append(l[0 : indice], l[indice + 1 : ]...)
	return removido
}

func (lista *ListaGenerica) RemoverInicio() interface{}{
	return lista.RemoverIndice(0)
}

func (lista *ListaGenerica) RemoverFim() interface{}{
	return lista.RemoverIndice(len(*lista) - 1)
}

func main() {
	dados := Lista{
		{1, 2},
		{2, 4},
		{3, 6},
	}

	fmt.Println("Dados : ", dados)

	lista := ListaGenerica{
		1, "Café", 42, true, 23, "Bola", 3.14, false,
	}

	fmt.Println("Lista original: ", lista)
	fmt.Println("Removendo do inicio: ", lista.RemoverInicio(), lista)
	fmt.Println("Removendo do fim: ", lista.RemoverFim(), lista)
	fmt.Println("Removendo o indice 3: ", lista.RemoverIndice(3), lista)

}
