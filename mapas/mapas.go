package main

import (
	"sort"
	"fmt"
	"time"
)

func main() {
	quadrados := make(map[int]int, 15)

	for n := 1; n <= 15; n++ {
		quadrados[n] = n * n
	}

	fmt.Println("Quadrados: ", quadrados)

	numeros := make([]int, 0, len(quadrados))

	start := time.Now()
	for n := range  quadrados {
		numeros = append(numeros, n)
	}

	fmt.Println("Numeros: ", numeros)

	sort.Ints(numeros)

	for _, numero := range numeros {
		quadrado := quadrados[numero]
		fmt.Printf("%d^2 = %d\n", numero, quadrado)
	}

	t := time.Now()

	fmt.Println("Tempo de execucao: ", t.Sub(start).Seconds())
}
