package main

import (
	"time"
	"fmt"
	"github.com/YasminGomes/teste/conjectura-collatz/collatz"
)

func main() {
	start := time.Now()
	passo, numero := collatz.MaiorCollatz(1, 1000000)
	fmt.Printf("O numero %d tem maior numero de passos, %d\n", numero, passo)
	fmt.Println("Tempo de execução:", time.Since(start).Seconds(), "segundos")

/*
	O numero 837799 tem maior numero de passos, 525
	Tempo de execução: 1.0410588
*/
}
