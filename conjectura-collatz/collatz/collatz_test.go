package collatz

import "testing"

func TestCollatz(t *testing.T) {
	for _, c := range []struct {
		in, passo int
		sequencia []int
	}{
		{5, 6, []int{5, 16, 8, 4, 2, 1}},
		{12, 10, []int{12, 6, 3, 10, 5, 16, 8, 4, 2 , 1}},
		{7, 17, []int{7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1}},
	}{
		got:= Collatz(c.in)
		if got != c.passo {
			t.Errorf("Collatz(%d): passo: %d == %d, sequencia: %d != %d", c.in, c.passo, got, c.sequencia)
		}
	}
}

func TestMaiorCollatz(t *testing.T) {
	for _, c := range []struct {
		inicio, fim, passo, numero int
	}{
		{5, 6, 9, 6},
		{10, 12, 15, 11},
		{20, 25, 24, 25},
	}{
		passo, numero := MaiorCollatz(c.inicio, c.fim)
		if passo != c.passo {
			t.Errorf("MaiorCollatz(%d, %d):\n\tMaior passo: %d == %d \n\tnumero: %d != %d", c.inicio, c.fim, c.passo, passo, c.numero, numero)
		}
	}
}
