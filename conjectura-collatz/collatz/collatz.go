package collatz

func Collatz(numero int) (int) {
	//sequencia := make([]int, 0)
	//sequencia = append(sequencia, numero)
	passo := 1
	for numero > 1 {
		if numero % 2 == 0 {
			numero = numero / 2
		}else {
			numero = numero * 3 + 1
		}
		passo++
		//sequencia = append(sequencia, numero)
	}
	return passo//, sequencia
}

func MaiorCollatz(inicio, fim int) (int, int) {
	maiorPasso := 0
	maiorNumero := inicio
	for i := inicio; i <= fim; i++ {
		passo := Collatz(i)
		if passo > maiorPasso {
			maiorPasso = passo
			maiorNumero = i
		}
	}
	return maiorPasso, maiorNumero
}
